Name:           google-carlito-fonts
Version:        1.1.03.beta1
Release:        1
Summary:        Sans-serif Font Metrics-compatible with Calibri
License:        OFL-1.1
Group:          System/X11/Fonts
# FIXME: This is likely not the best upstream URL. However, this font
#   is currently not available through Google Fonts.
Url:            https://commondatastorage.googleapis.com/chromeos-localmirror/distfiles/
Source:         https://commondatastorage.googleapis.com/chromeos-localmirror/distfiles/crosextrafonts-carlito-20130920.tar.gz
BuildRequires:  fontpackages-devel
# %reconfigure_fonts_prereq
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
BuildArch:      noarch

%description
Modern, friendly sans-serif font derived from the Lato font that is
designed to be a metrics-compatible drop-in replacement for Calibri.
Contains Regular, Bold, Italic, and Bold Italic version.

Designed by Lukasz Dziedzic of tyPoland for Google.

%prep
%setup -c %{name}

%build

%install
mkdir -p %{buildroot}%{_ttfontsdir}
# by default install command uses 755 umask
install -m 644 crosextrafonts-carlito-20130920/*.ttf %{buildroot}%{_ttfontsdir}/

# %reconfigure_fonts_scriptlets

%files
%defattr(-,root,root)
%doc crosextrafonts-carlito-20130920/LICENSE
%dir %{_ttfontsdir}
%{_ttfontsdir}/*

%changelog
